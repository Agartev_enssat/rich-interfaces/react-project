import React from "react";
import './Chapters.css'

export default class Chapters extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            chapters: props.chapters,
            video: props.video
        }

        this.setTime = this.setTime.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.chapters !== this.props.chapters) {
            this.setState({chapters: this.props.chapters});
        }
        if (prevProps.video !== this.props.video) {
            this.setState({video: this.props.video});
        }
    }

    render() {
        const chapters = this.state.chapters ?
            this.state.chapters.map(({pos, title}, index) =>
                <button className="chapter" key={index} rel="noopener noreferrer"
                   title="Cliquer pour aller au chapitre"
                   onClick={() => this.setTime(pos)}
                >
                    {title}
                </button>
            )
            :
            null;
        return (
            <div className="chapters">
                <span>Chapitres :</span>
                <div>{chapters}</div>
            </div>
        );
    }

    setTime(time) {
        if (this.state.video?.setTime) {
            this.state.video.setTime(time);
        }
    }
}