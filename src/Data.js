
export async function fetchData() {
    try {
        const res = await fetch("https://imr3-react.herokuapp.com/backend")
        const data = await res.json()
        return data;
    }
    catch (err) {
        console.log("failed to fetch film data", err)
    }
}