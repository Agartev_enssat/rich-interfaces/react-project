import React from "react";
import './Keywords.css'

export default class Keywords extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            keywords: props.keywords,
            keywordsPosNum: -1,
            video: props.video
        }

        this.timeUpdate = this.timeUpdate.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.keywords !== this.props.keywords) {
            this.setState({keywords: this.props.keywords});
        }
        if (prevProps.video !== this.props.video) {
            this.setState({video: this.props.video});
            if (this.props.video) {
                this.props.video.addEventListener('timeupdate', this.timeUpdate);
            }
        }
    }

    render() {
        if (this.state.keywordsPosNum > -1) {
            const keywords =
                this.state.keywords[this.state.keywordsPosNum].data
                    .map(({url, title}, index) =>
                        <a className="keyword" key={index} href={url} target="_blank" rel="noopener noreferrer">
                            {title}
                        </a>
                    );
            return (
                <div className="keywords">
                    <span>Mots-clés :</span>
                    <div>{keywords}</div>
                </div>
            );
        }
        else {
            return null;
        }
    }

    timeUpdate() {
        const currentPos = this.state.video ? this.state.video.currentTime : 0;
        let pos = 0, foundPosNum = 0;
        for (let index = 0; index < this.state.keywords.length; index ++) {
            pos = parseInt(this.state.keywords[index].pos);
            if (currentPos <= pos) {
                foundPosNum = index - 1;
                break;
            }
        }
        if (foundPosNum !== this.state.keywordsPosNum) {
            this.setState({keywordsPosNum: foundPosNum})
        }
    }
}