import React from "react";
// import './Timer.css'

export default class Timer extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            video: props.video,
            elapsedTime: this.convertSecondsToTimeString(0),
            totalTime: this.convertSecondsToTimeString(0)
        }

        this.timeUpdate = this.timeUpdate.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.video !== this.props.video) {
            this.setState({ video: this.props.video },
                () => {
                    if (this.state.video) {
                        this.state.video.addEventListener('timeupdate', this.timeUpdate);
                    }
                });
        }
    }

    render() {
        return (
            <div className="time">
                <span className="elapsed-time">{this.state.elapsedTime}</span>
                /
                <span className="total-time">{this.state.totalTime}</span>
            </div>
        );
    }

    timeUpdate() {
        const newObj = { elapsedTime: this.convertSecondsToTimeString(this.state.video.currentTime) };
        if (this.state.totalTime === "0:00") {
            newObj.totalTime = this.convertSecondsToTimeString(this.state.video.duration);
        }
        this.setState(newObj);
    }

    convertSecondsToTimeString(seconds) {
        const measuredTime = new Date(null);
        measuredTime.setSeconds(seconds); // specify value of SECONDS
        const isoTime = measuredTime.toISOString();
        // remove leading date and trailing milliseconds
        // and remove leading time zeros (ex: 00:00:05 => 0:05)
        return isoTime.match(/^[^T]*T(?:00:)?0?([^.]*)/)[1];
    }
}