import React from "react";
import './MessageForm.css';

export default class MessageForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            ws: props.ws,
            name: props.name,
            video: props.video,
            sendMoment: false
        }

        this.messageRef = React.createRef();
        this.submitMessage = this.submitMessage.bind(this);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.ws !== this.props.ws) {
            this.setState({ws: this.props.ws});
        }
        if(prevProps.name !== this.props.name) {
            this.setState({name: this.props.name});
        }
        if(prevProps.video !== this.props.video) {
            this.setState({video: this.props.video});
        }
    }

    render() {
        return (
            <form className="message-form" onSubmit={this.submitMessage}>
                <textarea className="message" ref={this.messageRef}></textarea>
                <div className="include-moment">
                    <input type="checkbox" id="include-moment" checked={this.state.sendMoment}
                           onChange={(event) => this.setState({sendMoment: event.target.checked})} />
                    <label htmlFor="include-moment">Inclure le moment du film</label>
                </div>
                <input type="submit" className="submit-message" value="Envoyer" disabled={this.state.ws ? "" : "disabled"}/>
            </form>
        );
    }

    submitMessage(event) {
        event.preventDefault();
        const messageString = this.messageRef.current.value;
        if (!messageString) {
            // this.messageRef.current
            return;
        }

        const message = { name: this.state.name, message: messageString };
        if (this.state.sendMoment) {
            message.moment = this.state.video.currentTime;
            this.setState({sendMoment: false});
        }
        this.state.ws.send(JSON.stringify(message));
        console.log(message);
        this.messageRef.current.value = "";
    }
}