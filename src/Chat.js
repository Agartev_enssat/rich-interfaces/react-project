import React from "react";
import './Chat.css';
import MessageForm from "./MessageForm";

const months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août',  'Septembre', 'Octobre', 'Novembre', 'Décembre'];

export default class Chat extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: props.name,
            messages: props.messages,
            ws: props.ws,
            video: props.video
        }

        this.addMessage = this.addMessage.bind(this);
        this.setMoment = this.setMoment.bind(this);
    }

    componentDidUpdate(prevProps) {
        const newObj = {};
        let updateState = false;
        if(prevProps.messages !== this.props.messages) {
            newObj.messages = this.props.messages;
            updateState = true;
        }
        if(prevProps.ws !== this.props.ws) {
            newObj.ws = this.props.ws;
            updateState = true;
        }
        if(prevProps.name !== this.props.name) {
            newObj.name = this.props.name;
            updateState = true;
        }
        if(prevProps.video !== this.props.video) {
            newObj.video = this.props.video;
            updateState = true;
        }
        if (updateState) {
            this.setState(newObj);
        }
    }

    render() {
        let currentDay = new Date(null), date = null, newHour = null, newDay = null;
        const messages = this.state.messages
            .map(({message, name, moment, when}, index) => {
                try {
                    date = new Date(when);
                }
                catch (e) {
                    date = new Date();
                }
                if (typeof(moment) !== 'number' && (moment < 0 || moment > 1000 * 1000 * 1000)) {
                    moment = 0;
                }
                if (typeof(when) !== 'number' && (when < 0 || when > 1000 * 1000 * 1000)) {
                    when = Date.now();
                }
                // séparateur avec la date du message si le précédent est d'un jour différent
                newDay = date.getDay() !== currentDay.getDay() ?
                    <div className="new-day">
                        {date.getDay()} {months[date.getMonth()]} {date.getFullYear()}
                    </div>
                    :
                    null;
                // séparateur avec l'heure du message si le précédent est plus ancien qu'une heure
                newHour = currentDay.getTime() + 60 * 60 * 1000 < date.getTime() ?
                    <div className="new-hour">
                        {date.getHours()}:{date.getMinutes()}
                    </div>
                    :
                    null;
                currentDay = date;
                name = name ? name.charAt(0).toUpperCase() + name.slice(1) : 'Anonyme';
                return (
                    <React.Fragment key={index}>
                        {newDay}
                        {newHour}
                        <div className={`message ${this.state.name === name ? 'yourself' : ''}`} tabIndex={index} >
                            <span className="name">{name.charAt(0)}</span>
                            <div className="content">{message}</div>
                            {typeof(moment) !== 'undefined' ?
                                <div className="moment" onClick={() => this.setMoment(moment)}>M</div>
                                :
                                null
                            }
                        </div>
                        <div className="time">{name} à {date.toISOString().substring(11, 16)}</div>
                    </React.Fragment>
                );
            });
        return (
            <aside className="chat">
                <div className="pseudo">{this.state.name}</div>
                <div className="messages" ref={(ref) => { if (ref?.childElementCount > 0 && !this.scrolled) { this.scrolled = true; ref.scrollTop = ref.scrollHeight; } } }>
                    {messages}
                </div>
                <MessageForm ws={this.state.ws} name={this.state.name} video={this.state.video} addMessage={this.addMessage} />
            </aside>
        );
    }

    addMessage(message) {
        this.state.messages.push(message);
        this.setState({messages: this.state.messages});
    }

    setMoment(moment) {
        this.state.video.setTime(moment);
        this.setState({ video: this.state.video });
        console.log(moment)
    }
}
