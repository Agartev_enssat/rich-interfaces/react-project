import React from "react";
import { MapContainer, TileLayer, Marker, Popup, useMap } from 'react-leaflet';
import './Map.css'

export default class Map extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            waypoints: props.waypoints,
            center: [51.505, -0.09],
            video : props.video,
        }

        this.setMoment = this.setMoment.bind(this);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.waypoints !== this.props.waypoints) {
            this.setState({ waypoints: this.props.waypoints },
                this.setupWaypoints);
        }
        if (prevProps.video !== this.props.video) {
            this.setState({ video: this.props.video })
        }
    }

    render() {
        return (
            <aside className="map">
                <MapContainer center={this.state.center} zoom={3} scrollWheelZoom={true}>
                    <TileLayer
                        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                    />
                    {this.state.waypoints.map((waypoint, index) =>
                        <Marker position={[waypoint.lat, waypoint.lng]} key={index}
                            eventHandlers={{
                                click: () => {
                                    this.setMoment(waypoint.timestamp);
                                }
                            }}
                        >
                            <Popup>
                                {waypoint.label}
                            </Popup>
                        </Marker>
                    )}
                    <ChangeMapView coords={this.state.center} />
                </MapContainer>
            </aside>
        )
    }

    setupWaypoints() {
        if (this.state.waypoints.length !== 0) {
            const center = [0, 0];
            this.state.waypoints.forEach(waypoint => {
                center[0] += parseInt(waypoint.lat);
                center[1] += parseInt(waypoint.lng);
            })
            center[0] /= this.state.waypoints.length;
            center[1] /= this.state.waypoints.length;
            this.setState({ center });
            console.log('map center', center);
        }
    }

    setMoment(moment) {
        if(this.state.video) {
            this.state.video.setTime(moment);
            console.log(moment)
        }
    }
}

function ChangeMapView({ coords }) {
    const map = useMap();
    map.setView(coords, map.getZoom());

    return null;
}