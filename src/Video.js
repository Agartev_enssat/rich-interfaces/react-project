import React from "react";
import './Video.css';
import Timer from "./Timer";

/********************************************************************
 basé sur le code de cet article :
 https://css-tricks.com/custom-controls-in-html5-video-full-screen
 *********************************************************************/

export default class Video extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            src: props.src,
            volume: 0.2,
            time: 0,
            video: null,
        }

        this.setVideo = this.setVideo.bind(this);
    }

    componentDidUpdate(prevProps) {
        if(prevProps.src !== this.props.src) {
            this.setState({src: this.props.src});
        }
    }

    render() {
        return (
            <div className="player" ref={this.setVideo} >
                <video preload="metadata">
                    {this.state.src ? <source src={this.state.src} /> : null}
                    <p>Your browser does not support the HTML5 Video Element</p>
                </video>
                <div className="controls">
                    <button className="playpause icon-play"/>
                    <div className="seeker">
                        <progress className="progressbar" max='100' value='0'/>
                        <input type="range" value={this.state.time} className="seekbar" onInput={(e) => this.setTime()}/>
                    </div>
                    <Timer video={this.state.video} />
                    <button className="mute icon-volume-2"/>
                    <div className="volume">
                        <input type="range" className="volumebar" value={this.state.volume} min="0" max="1" step="0.1" onInput={() => this.setVolume()} />
                    </div>
                    <button className="fullscreen icon-fullscreen-alt" />
                </div>
                {this.props.children}
            </div>
        )
    }

    setVideo(element) {
        if (element) {
            const video = element.querySelector('video'),
                // container = element.parentNode,
                playbutton = element.querySelector('.playpause'),
                mutebutton = element.querySelector('.mute'),
                fullscreenbutton = element.querySelector('.fullscreen'),
                seek = element.querySelector('.seekbar'),
                volume = element.querySelector('.volumebar'),
                progressbar = element.querySelector('.progressbar');
            let vval = video.volume = volume.value;
            video.setTime = function (time) {
                video.currentTime = time;
            }

            if (video.autoplay) {
                playbutton.classList.add("icon-pause");
                playbutton.classList.remove("icon-play");
            }
            video.addEventListener("playing", function () {
                seek.classList.add("light");
            }, false);

            if (video.muted) {
                mutebutton.classList.add("icon-volume");
                mutebutton.classList.remove("icon-volume-2");
                volume.value = 0;
            } else {
                mutebutton.classList.add("icon-volume-2");
                mutebutton.classList.remove("icon-volume");
            }

            function playpause() {
                if (video.paused) {
                    video.play();
                    playbutton.classList.add("icon-pause");
                    playbutton.classList.remove("icon-play");
                    seek.classList.add("light");
                } else {
                    video.pause();
                    playbutton.classList.add("icon-play");
                    playbutton.classList.remove("icon-pause");
                    seek.classList.remove("light");
                }
            }

            playbutton.addEventListener("click", playpause, false);
            video.addEventListener("click", playpause, false);

            mutebutton.addEventListener("click", function () {
                if (video.muted) {
                    video.muted = false;
                    mutebutton.classList.add("icon-volume-2");
                    mutebutton.classList.remove("icon-volume");
                    volume.value = vval;
                } else {
                    video.muted = true;
                    volume.value = 0;
                    mutebutton.classList.add("icon-volume");
                    mutebutton.classList.remove("icon-volume-2");
                }
            }, false);

            let isFullscreen = false;
            fullscreenbutton.addEventListener("click", function () {
                if (!isFullscreen) {
                    // if (container.requestFullscreen) {
                    //     container.requestFullscreen();
                    // } else if (container.mozRequestFullScreen) {
                    //     container.mozRequestFullScreen(); // Firefox
                    // } else if (container.webkitRequestFullscreen) {
                    //     container.webkitRequestFullscreen(); // Chrome and Safari
                    // }
                    if (document.body.requestFullscreen) {
                        document.body.requestFullscreen();
                    } else if (document.body.mozRequestFullScreen) {
                        document.body.mozRequestFullScreen(); // Firefox
                    } else if (document.body.webkitRequestFullscreen) {
                        document.body.webkitRequestFullscreen(); // Chrome and Safari
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    }
                }
            }, false);
            document.addEventListener('fullscreenchange', function () {
                console.log(isFullscreen)
                if (!isFullscreen) {
                    isFullscreen = true;
                    fullscreenbutton.classList.remove("icon-fullscreen-alt");
                    fullscreenbutton.classList.add("icon-fullscreen-exit-alt");
                    document.body.classList.add('fullscreen')
                } else {
                    isFullscreen = false;
                    fullscreenbutton.classList.add("icon-fullscreen-alt");
                    fullscreenbutton.classList.remove("icon-fullscreen-exit-alt");
                    document.body.classList.remove('fullscreen');
                }
            })

            //change video time when seek changes
            // seek.addEventListener("change", function() {
            this.setTime = function () {
                const val = seek.value;
                video.currentTime = video.duration * val / 100.0;
                this.setState({time: val});
            }
            // this.setTime = this.setTime.bind(this)
            // }, false);

            let mousemove = null;
            seek.addEventListener("mousedown", function () {
                video.pause();
                seek.addEventListener('mousemove', mousemove = updateProgressbar)
            }, false);
            seek.addEventListener("mouseup", function () {
                video.play();
                //if the user plays the video without clicking play, by starting directly with specifying a point of time on the seekbar, make sure the play button becomes a pause button
                playbutton.classList.remove("icon-play");
                playbutton.classList.add("icon-pause");
                seek.removeEventListener('mousemove', mousemove);
            }, false);

            //update progress bar as video plays
            function updateProgressbar() {
                const percent = Math.floor(100 / video.duration * video.currentTime);
                progressbar.value = percent;
                if (progressbar.getElementsByTagName("span")[0]) {
                    progressbar.getElementsByTagName("span")[0].innerHTML = percent;
                }
                //change seek position as video plays
                seek.value = percent;
            }

            video.addEventListener("timeupdate", updateProgressbar, false);

            // volume.addEventListener("change", function() {
            this.setVolume = function () {
                vval = volume.value;
                video.volume = vval;
                if (vval === "0") {
                    video.muted = true;
                    mutebutton.classList.add("icon-volume");
                    mutebutton.classList.remove("icon-volume-2");
                } else {
                    video.muted = false;
                    mutebutton.classList.add("icon-volume-2");
                    mutebutton.classList.remove("icon-volume");
                }
                this.setState({volume: vval});
            }
            // }, false);

            video.addEventListener("ended", function () {
                video.pause();
                video.currentTime = 0;
                playbutton.classList.add("icon-play");
                playbutton.classList.remove("icon-pause");
                seek.classList.remove("light");
            });

            video.addEventListener('keypress', function (event) {
                if (event.code === "Space") {
                    playpause();
                }
            });

            this.setState({video});
            this.props.setVideo(video);
        }
    }
}
