import React from "react";
import './Film.css'
import Video from "./Video";
import Keywords from "./Keywords";
import Chapters from "./Chapters";

export default class Film extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            film: props.film,
            chapters: props.chapters,
            keywords: props.keywords,
            showSynopsis: false,
            video: null
        }

        this.toggleSynopsis = this.toggleSynopsis.bind(this);
        this.setVideo = this.setVideo.bind(this);
    }

    componentDidUpdate(prevProps) {
        const newProps = {}
        let updateState = false;
        if (prevProps.film !== this.props.film) {
            newProps.film = this.props.film;
            updateState = true;
        }
        if (prevProps.chapters !== this.props.chapters) {
            newProps.chapters = this.props.chapters;
            updateState = true;
        }
        if (prevProps.keywords !== this.props.keywords) {
            newProps.keywords = this.props.keywords;
            updateState = true;
        }
        if (updateState) {
            this.setState(newProps);
        }
    }

    render() {
        return (
            <section className="video">
                <h1 className="title">{this.state.film?.title}</h1>
                <Video src={this.state.film?.file_url} setVideo={this.setVideo}>
                    <div className="synopsis">
                        <div>
                            <h2>Synopsis</h2>
                            <div id="toggle-synopsis" className={this.state.showSynopsis ? 'active' : ''} onClick={this.toggleSynopsis}
                               title={'Cliquer pour ' + (this.state.showSynopsis ? 'réduire' : 'agrandir')} />
                            <div id="text">
                                {/*this.state.data?.Film?.synopsis_url*/}
                                Three German provincials are searching for hollywood.
                                So they take the most American values, they can picture:
                                an absurdly vast car, full of fastfood,
                                bottles of oil and camera equipment on the way to California via Route 66.
                                That they never find Hollywood is not the only surprise -
                                on their quest for the American cliches they face unplanned incidents.
                                A documentary drama somewhere between Jackass and Michael Moore,
                                between gloating and satire.
                            </div>
                        </div>
                    </div>
                </Video>
                <Chapters chapters={this.state.chapters} video={this.state.video} />
                <Keywords keywords={this.state.keywords} video={this.state.video} />
            </section>
        );
    }

    toggleSynopsis() {
        this.setState({showSynopsis: !this.state.showSynopsis});
    }

    setVideo(video) {
        this.setState({video});
        this.props.setVideo(video);
    }
}