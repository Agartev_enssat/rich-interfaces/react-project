import './App.css';
import { ReactComponent as Logo } from './logo.svg';
import Film from "./Film";
import Chat from "./Chat";
import Map from "./Map";
import React from "react";
import { fetchData } from "./Data";

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: {
                Chapters: [],
                Film: [],
                Keywords: [],
                Waypoints: [],
                Messages: [],
            },
            connected: false,
            name: "",
            video: null
        }

        this.setVideo = this.setVideo.bind(this);
        this.setName = this.setName.bind(this);
        this.setupWebSocket = this.setupWebSocket.bind(this);
    }

    componentDidMount() {

        fetchData()
            .then(data => this.setState({ data: Object.assign(this.state.data, data) }));

        this.setName(localStorage.name);

        this.setupWebSocket();
    }

    render() {
      return (
        <div className="App">
          <header className="header">
            <Logo className="logo" alt="logo" />
            <span className="title">Film&Chat</span>
            <button className="pseudo" alt="Cliquer pour changer votre pseudo" onClick={() => this.setName()}>{this.state.name}</button>
          </header>
          <article className="container">
            <Film film={this.state.data.Film} chapters={this.state.data.Chapters} keywords={this.state.data.Keywords} setVideo={this.setVideo} />
            <Chat name={this.state.name} messages={this.state.data.Messages} ws={this.state.ws} video={this.state.video} />
            <Map waypoints={this.state.data.Waypoints} video={this.state.video}/>
            <footer className="footer">
              <span className="copyright">© Steven Toutant & Dorian Souleyreau</span>
              <span className="infos">CC 4.0 : BY/SA</span>
            </footer>
          </article>
        </div>
      );
    }

    setupWebSocket() {
        const URL = "wss://imr3-react.herokuapp.com";
        let ws = new WebSocket(URL);

        ws.onopen = () => {
            console.log("connected");
            this.setState({connected: true, ws});
        };

        ws.onmessage = event => {
            try {
                const data = JSON.parse(event.data);
                console.log(data);
                // charger toutes les données au démarrage (que la liste est vide)
                // et ne charger que la dernière en date ensuite
                if (this.state.data.Messages.length > 0) {
                    // en cas de deconnexion/reconnexion, les données sont renvoyées
                    // donc le dernier message fait doublon
                    // ceci évite cela
                    const newData = data.pop();
                    if (!this.state.data.Messages.find(obj => obj.when === newData.when)) {
                        this.state.data.Messages.push(newData);
                    }
                }
                else {
                    this.state.data.Messages.push(...data);
                }
                this.setState({data: this.state.data});
            }
            catch (err) {
                console.log('failed to parse message data', err);
            }
        };

        ws.onclose = () => {
            console.log("disconnected, reconnect.");
            ws = null;
            this.setState({connected: false,ws});
            this.setupWebSocket();
        };
    }

    setVideo(video) {
        this.setState({video});
    }

    setName(userName) {
        while (!userName) {
            userName = prompt("Bonjour, quel est vote nom ?");
            localStorage.name = userName ?? "";
        }
        this.setState({ name: userName });
    }
}

export default App;

