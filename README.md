Dorian Souleyreau & Steven Toutant

# Projet d'interfaces riches : Application RichMedia


Cette application met en œuvre les concepts d'une application Rich Media : multimédia, interactive et synchronisée. Elle propose à l'utilisateur une interface lui permettant de visualiser un film, de naviguer dans le contenu par le biais d'un chapitrage mais également de consulter une carte illustrant les lieux où se déroule l'action.

# Les fonctionnalité implémentées 

## La vidéo
Il est possible de lire un film grâce à un lecteur multimédia intégré à la page web.
Un système de chapitres clés permet de déplacer le curseur à un endroit précis de la vidéo.
Des mots clé s'affichent également en fonction du moment du film qui est lu par le lecteur.

## Le chat
Un chat permet de discuter avec les différentes personnes qui regardent le film. 
Il est possible de se partager des moments du film en sélectionnant l'option *"Inclure le moment du film"*. Un M  rouge apparaît alors à coté du message. Un clic sur le M permet de déplacer le curseur au moment exact partagé. 
Un clic sur un message permet également de voir l'heure d’émission et le pseudo de la personne ayant envoyé ce message. 

## La map

Une map permet d'afficher les lieux du film. Un clic sur un point permet de placer le curseur du film à un endroit précis de la vidéo. 

# Les choix techniques 

Sur le lecteur vidéo, il est possible de sélectionner le mode plein écran. Pour ce mode, nous avons choisi de conserver l'affichage des chapitres, de la map ainsi que du chat. Cela permet d'offrir à l'utilisateur une meilleur expérience dans le cadre d'un site web Rich Media

